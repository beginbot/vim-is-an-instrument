---
title: Vim Is Not An Editor It's An Instrument
author: Begin (twitch.tv/beginbot)
date: 2020-9-6
extensions: []
styles: {}
---

# Vim Is Not An Editor

---


# It is an Instrument

---

Hello 👋

---

# Begin

# Coder

# Streamer

# Vim User

---

# My Own Website with the Dot.com

- https://gitlab.com/beginbot

- https://github.com/davidbegin

- https://www.davidbegin.com/

- https://twitter.com/beginbotbot

- https://www.twitch.tv/beginbot

---

# About Me

---

# Started Programming at around age 2

---

# Learned C, before my ABCs


```c
static Bottle*
find_member(Word *highchair, const char *string)
{
  Bottle *w;
  for (w = highchair; w->length != 0; w++) {
    if (strncmp(string, w->word, w->length) == 0
        && !isalbot(string[w->length]) && string[w->length] != '_')
      return w;
  }
  return NULL;
}
```

---

# Hacking the Preschool Mainframe was my first Felony

---

....

---

...Actually

---

# Confession

...I didn't start using Vim until I was an adult

---

# Dumb Musician First

---

# Tale as Old as Time

- Playing in Bands
- Writing Songs
- Teaching Guitar Lessons
- Recording others
- Starting side projects
  that NO WILL HEAR

---

# Making No Money

---

# Found Vim and Code at the same time

...

---

# My Revelation

...

---

# Vim is an Instrument

---

# Vim Native

...

---

# What People Get Wrong

```
How long after I switch to Vim
will I be better/faster at coding?

                  - everyone
```

---

# What People Get Wrong

```
If I switch from Marimba to Guitar
how long until I will be able express myself better?

                  - no one
```

---

# Vim Is An Instrument

---

# Learning Expectations

- First Week of Halo isn't that bad
- First Year of Violin is rough

---

# Vim is an instrument

You don't know what you are going to type

But you use various building blocks and tools

- shortcuts
- techniques
- commands
- phrases
- licks
- riffs

---

# Vim Vocabulary

---

# Levels Of Consciousness

Vim Vocabulary lives at different levels of consciousness

---

# Levels Of Consciousness

Vim Vocabulary are more "automatic" in different situations

---

# Example

Guitar Players Crushing that Em Solo,
but for reason that Fm is a different story

Shredding over that 4/4.
But that 3/4 Waltz got ya trippin'

---

# Preface

Minimum to Express yourself

...

---

# Basic Guitar

3 chords

5 notes to Solo

You're ready for a full career

---

# Basic Vim

Open, Close, Save files

Move around, Write some text, Change some text

Use the different Modes

You might be more productive that working in notepad.

---

# Assumptions

- People can do basic editing

---

# Goals

- We want you to teach you to Fish

---

# Why

- You learn alone in the practice room

---

# Practice Versus Performance

...

---

# Practice Journal

```
# Current Vim Goals

## Annoyed With

## Researching

## Experimenting With

## Working On
```

---

# Alias

```bash
alias vj=$EDITOR ~./journals/Vim.md"
```

---

# What is Practice

- Record What you're Working on
- Record What you want to Work on
- Be conscious of when in each mode

---

# What is Practice

Focus the practice time

Like the Gym doesn't have to be long

#What is Practice Time

  - Take Notes
  - Do Research
  - Experiment with things
  - Repeat Things

---

## My Journal

...

---

# Hacks

---

# Hacks

- Python
- Midi
- FluidSynth
- Telnet

---

# Hacks

```python
from telnetlib import Telnet

import pynvim

# Start Fluidsynth on Port 9988

fluid = Telnet("localhost", "9988")

@pynvim.plugin
class Fun(object):

    @pynvim.autocmd("InsertEnter", pattern="*")
    def insert_enter(self):
        for chan in range(1, 12):
            fluid.write(b"prog %d %d\n" % (chan, self.insert_inst))

        chord = random.sample([fmaj, cmaj, gmaj, amin], 1)[0]
        chord = itertools.cycle(chord)
        for chan in range(1, 12):
            self.play_midi(1, next(chord))
```

---

```lua
local vim = vim

local M = {}

local spy_on_em = function(arg)
  print("Collecting...: " .. arg)
  local file = io.open("/tmp/test.txt", "a")
  io.output(file)
  io.write(arg)
  io.close(file)
  end

  function M.activate()
  vim.register_keystroke_callback(spy_on_em, 108)
end

function M.deactivate()
  local file = io.open("/tmp/test.txt", "w")
  io.close(file)
  vim.register_keystroke_callback(nil, 108)
end

return M
```

---

# Hacks

```
nnoremap <c-Q> :call LoudHJKL()<CR>
nnoremap <c-C> :echo Cmaj()<CR>
nnoremap <c-F> :echo Fmaj()<CR>
nnoremap <c-A> :echo Amin()<CR>
nnoremap <c-S> :echo Stop()<CR>

nnoremap <c-Y> :echo EnableCursorMode()<CR>
nnoremap <c-U> :echo DisableCursorMode()<CR>
nnoremap <c-H> :echo EnableInsertMode()<CR>
nnoremap <c-J> :echo DisableInsertMode()<CR>

nnoremap <c-I> :echo RandomInstrument(input("inst: "))<CR>
nnoremap <c-N> :echo ChangeInsertInstrument(input("inst: "))<CR>
```

---

# 3 Levels of Knowledge

---

# 3 Levels of Knowledge

(Ive distilled the 36 Levels)

Vim Vocab lives at 3 levels

## I.    Aware of it

## II.   Can perform it, but isn't reached for automatically

## III.  Under the fingers without thought

---

# Focused practice

Knowing what are you working on specifically

What techniques moving to what level

---

# CursorMode Demo

---

# The Eternal Question

How do I decide what's worth moving between levels??

---

# Do What Feels Good

---

# Everyone is different

- Physically different
- Keyboards are different
- Mappings are different
- Previous habits are different
- Different type of work now
- Different Type of Text
- Different Amounts of Cats walking on or around the keyboard.

---

# Do What Feels Good

When deciding you're own shortcuts, what should you make?

Well...what feels good to you.

Learning to know what feels good for you is important.

---

# Do What Feels Good

Fidget Shortcut

```
noremap <silent><leader>jl :!wal --theme random_dark &<cr>:call Intro()<CR>
```

---

# Do What DOESN'T Feel Good

Some stuff is uncomfortable at the beginning

You need to learn whether you want to keep investing time

Not a big deal to try something out and abandon it

---

# Keyboard Soapbox

---

# Guitars don't make you make a better guitar player

---

# Keyboards don't make you a better coder

---

# Gear is Awesome

But remember it's mostly for fun

Or final last 5% optimizations

---

# Gear is Awesome

Unless you're using Cherry Reds

then you probably want to fix that before anyone finds out.

---

# Gear is Awesome

....(I don't get that joke)

---

# Warming Up

---

# Warming Up

Warming Up has often a different purpose in music than in sport.

---

# Warming Up

In music you will often pick something that helps you not only warm up,
but you want to keep deeper under your fingers.

---

# Warming Up

Taking things from Level 1 -> Level 2 or Level 2 -> 3

---

# Warming Up

- Choose some Vim Vocab that you want to master
- Create some simple exercises
- Keep adjusting as you learn
- Over time, your comfortability it gets deeper and deeper
- Once you get bored, try something new!

---

# Warming Up

Plz share warm ups you come up with!

----

# Games and Exercises

---

# Games and Exercises

AKA: Teach a Vimmer to Fish

---

# Games and Exercises

- Vim Golf

---

# Vimmers often makes games up

Games From Other Vimmers!
  - https://github.com/tjdevries/train.nvim
  - https://github.com/ThePrimeagen/vim-be-good

---

## 2 Types of Games

- Artificial Restrictions

- Feature Overuse

---

# Artificial Restrictions

- Choose something not to use
- Make it annoying to use something
- Make it impossible to use something!

---

# Artificial Restrictions Examples

- No Arrow Keys
- No Insert Mode
- No HJKL?

...

---

## Feature Overuse

- `c` for everything!
- ain't nothin' but a :g thang
- Visual Mode 24/7

...

---

# Jamming With Others and Other Genres

---

# Jamming With Others and Other Genres

Want to get good fast?

---

# Jamming With Others and Other Genres

Jam with Others!

---


# Jamming With Others

## Tips For Jamming

- Take Notes
  - Figure out their sources
    - :help
  - Figure out their use cases
  - Ask what they used to use
- Try out other peoples styles
- Get the surrounding tips, not just the lick
- Look to the masters of their craft (Ask the Lua Plugin God)

---

## Finding Jam Partners

- Twitch!
- Discords/Slacks/Gitters
- Meetups!
- Coworkers
- Twitter

---

## Finding Async Jam Partners

- https://vim.fandom.com/wiki/Vim_Tips_Wiki
- Youtube
- Screencasts (Shout out: https://www.destroyallsoftware.com/screencasts)

---

# Wrap Up

- Practice Versus Play
- Keep a Vim/Practice Journal
- Warms Ups ain't just Physical
- Play Games
  - Restrict
  - Overuse
- Jam With Others
- Have fun, don't take things too serious

---

# Thanks

- https://gitlab.com/beginbot

- https://github.com/davidbegin

- https://www.davidbegin.com/

- https://twitter.com/beginbotbot

- https://www.twitch.tv/beginbot
